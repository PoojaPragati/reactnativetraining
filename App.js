/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
  View,
  Text,
  TextInput,
  Button,
} from 'react-native';

class App extends React.Component{
constructor(props){
  super(props);
  this.state = {
    email:"",
  }
}
handleChange = (email) => {
  this.setState({ email });
}
submitButton = () => {
    const {email}=this.state;
    var emailformat=/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
    if(email.match(emailformat))
    {
       window.alert("Given email is valid");
    }
    else{
       window.alert("Given email is invalid");
    }
}
  render(){
    return (
      <View>
        <Text style={{textAlign:'center'}}>Assignment 1</Text>
              <TextInput placeholder="Enter Email" style={{borderColor:'skyblue',margin:15,borderWidth:2}} value={this.email}
               onChangeText={this.handleChange}     />
              <Button type="submit" title="Test" onPress={this.submitButton}/>
          </View>

    )
  }
}
export default App;